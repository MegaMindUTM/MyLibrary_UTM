# Change Log
All notable changes to this project will be documented in this file.
 
The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).
 
## [Release] - 2021-2-1
 
Here we write upgrading notes for brands. It's a team effort to make them as
straightforward as possible.
 
### Added
- [MyLibrary@UTM](https://gitlab.com/MegaMindUTM/MyLibrary_UTM)
 
### Changed
This is first release. No Changes

### Fixed
 [V1.0] - 2021-02-1
 Here we would have the update steps for 1.2.4 for people to follow.