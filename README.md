# MyLibrary@UTM

A Library Management System with PHP and MySQL. MyLibrary@UTM is a smart library website that enable the users, which are the students and staffs of the UTM campus for the borrowing and returning the books just through website without have to write up the book detail in the book provided.Besides, the libarary website helps Admin (Librarian) to manages all the services that UTM Library provided.​ 

## Purpose of the Project
Analyze, specify, design, implement, document and demonstrate an information
system application to support a library.MyLibrary@UTM will be an ambitious development where this application would be the pioneering project that going to change the whole process of the campus residents benefitted on services that currently provided from the main libraries inside the campus. In general, the system that we develop are a smart library website that enable the users, which are the students and staffs of the UTM campus for the borrowing and returning the books just through website without have to write up the book detail in the book provided. Besides that, this website can show the book catalogue and also their book details including the availability of the book. Hence student do not have to search the book in the library shelves because student can just choose look through the book catalogue and request to borrow. While for librarian this website helps them to track the data, status and record of the borrowing and returning books. Then, librarian also enable to deliver reminders or notifies the borrowers through the website. Therefore, this website does ease librarian and student in order to borrowing and manage the library services.


## Authors

* **Abu Bakar Bin Ahmad Bashir** - *Project Manager* - [Profile](https://gitlab.com/abou_bakrx)
* **Muhammad Amirul Firdaus Bin Mat Arif Shah** - *Programmer and Database Manager* - [Profile](https://gitlab.com/Amrlfs)
* **Aina Syahmie Binti Rahmat** - *UI/UX Design Coordinator* - [Profile](https://gitlab.com/ainasyahmie)
* **Nur Alia Binti Mohd Syehab** - *Quality Assurance (Testing) Analyst* - [Profile](https://gitlab.com/aliasyehab98)


## Built With

* [Bootstrap](https://getbootstrap.com/docs/5.0/getting-started/introduction/) - The web frontend framework used
* [jQuery](https://blog.jquery.com/) - The web backend framework used
* [Apache & MySQL by Wampserver](https://www.wampserver.com/en/) - Server and database management


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details


 
