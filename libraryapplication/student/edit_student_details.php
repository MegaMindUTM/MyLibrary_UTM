<?php
ob_start();
require('dbconn.php');
?>


<!DOCTYPE html>
<html lang="en">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>MyLibrary@UTM</title>
        <link type="text/css" href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link type="text/css" href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet">
        <link type="text/css" href="css/theme.css" rel="stylesheet">
        <link type="text/css" href="images/icons/css/font-awesome.css" rel="stylesheet">
        <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600'
            rel='stylesheet'>
    </head>
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".navbar-inverse-collapse">
                        <i class="icon-reorder shaded"></i></a><a class="brand" href="index.php"><img style="display: inline; width:90px;" src="images/librarylogo.png" alt="logo"/>
<h1 style="display: inline;"></h1></a>
                    <div class="nav-collapse collapse navbar-inverse-collapse">
                        <ul class="nav pull-right">
                            <li class="nav-user dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="images/user.png" class="nav-avatar" />
                                <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="index.php">Your Profile</a></li>
                                    <!--li><a href="#">Edit Profile</a></li>
                                    <li><a href="#">Account Settings</a></li-->
                                    <li class="divider"></li>
                                    <li><a href="logout.php">Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- /.nav-collapse -->
                </div>
            </div>
            <!-- /navbar-inner -->
        </div>
        <!-- /navbar -->
        <div class="wrapper">
            <div class="container">
                <div class="row">
                    <div class="span3">
                        <div class="sidebar">
                            <ul class="widget widget-menu unstyled">
                                <li class="active"><a href="index.php"><i class="menu-icon icon-home"></i>Home
                                </a></li>
                                 <li><a href="message.php"><i class="menu-icon icon-inbox"></i>Messages</a>
                                </li>
                                <li><a href="book.php"><i class="menu-icon icon-book"></i>All Books </a></li>
                                <li><a href="history.php"><i class="menu-icon icon-tasks"></i>Previously Borrowed Books </a></li>
                                <li><a href="recommendations.php"><i class="menu-icon icon-list"></i>Recommend Books </a></li>
                                <li><a href="current.php"><i class="menu-icon icon-list"></i>Currently Issued Books </a></li>
                            </ul>
                            <ul class="widget widget-menu unstyled">
                                <li><a href="logout.php"><i class="menu-icon icon-signout"></i>Logout </a></li>
                            </ul>
                        </div>
                        <!--/.sidebar-->
                    </div>
                    <!--/.span3-->
                    <div class="span9">
                        <div class="module">
                            <div class="module-head">
                                <h3>Update Details</h3>
                            </div>
                            <div class="module-body">


                              <?php
                              $rollno = $_SESSION['RollNo'];
                              $sql="select * from LMS.user where RollNo='$rollno'";
                              $result=$conn->query($sql);
                              $row=$result->fetch_assoc();

                              $name=$row['Name'];
                              $category=$row['Category'];
                              $email=$row['EmailId'];
                              $mobno=$row['MobNo'];
                              $pswd=$row['Password'];
                              ?>


                                <form class="form-horizontal row-fluid" action="edit_student_details.php?id=<?php echo $rollno ?>" method="post">

                                    <div class="control-group">
                                        <label class="control-label" for="Name"><b>Name:</b></label>
                                        <div class="controls">
                                            <input type="text" id="Name" name="Name" value= "<?php echo $name?>" class="span8" required>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                            <label class="control-label" for="Category"><b>Category:</b></label>
                                            <div class="controls">
                                                <select name = "Category" tabindex="1" value="SC" data-placeholder="Select Category" class="span6">
                                                    <option value="<?php echo $category?>"><?php echo $category ?> </option>
                                                    <option value="GEN">General</option>
				                                    <option value="FE">FE</option>
					                                <option value="FS">FS</option>
					                                <option value="FB">FB</option>
                                                </select>
                                            </div>
                                    </div>


                                    <div class="control-group">
                                        <label class="control-label" for="EmailId"><b>Email Id:</b></label>
                                        <div class="controls">
                                            <input type="text" id="EmailId" name="EmailId" value= "<?php echo $email?>" class="span8" required>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="MobNo"><b>Mobile Number:</b></label>
                                        <div class="controls">
                                            <input type="text" id="MobNo" name="MobNo" value= "<?php echo $mobno?>" class="span8" required>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="Password"><b>New Password:</b></label>
                                        <div class="controls">
                                            <input type="password" Name="Password" id="Password" value= "<?php echo $pswd?>" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
                                            title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" onkeyup='check();' class="span8" required>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="Password"><b>Confirm Password:</b></label>
                                    <div class="controls">
                                        <input type="password" Name="Confirmpassword" id="Confirmpassword" placeholder="Confirm Password" onkeyup='check();' class="span8" required>
                                        <span id='message'></span>
                                    </div>
                                    </div>

                                    <div class="control-group">
                                            <div class="controls">
                                                <button type="submit" name="submit"class="btn-primary"><center>Update Details</center></button>
                                            </div>
                                        </div>

                                    </form>

                                    </div>
                                    </div>
                                    </div>

                                    <!--/.span9-->
                                    </div>
                                    </div>
                                    <!--/.footer-->
                                    <footer>
                                    <div class="brands" >
                                    <a href="#" > <img src="images\MegaMind.png" style="width: 142px;"  >
                                    <img src="images\utm.png" style="width: 102px;">
                                    <img src="images\utmlibinfo.png" style="width: 230px;"></a>
                                    </div>
                                    </footer>

                                    <!--/.wrapper-->
                                    <script src="scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
                                    <script src="scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
                                    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
                                    <script src="scripts/flot/jquery.flot.js" type="text/javascript"></script>
                                    <script src="scripts/flot/jquery.flot.resize.js" type="text/javascript"></script>
                                    <script src="scripts/datatables/jquery.dataTables.js" type="text/javascript"></script>
                                    <script src="scripts/common.js" type="text/javascript"></script>
                                    <script>
                                    var check = function() {
                                    if (document.getElementById('Password').value ==
                                    document.getElementById('Confirmpassword').value) {
                                    document.getElementById('message').style.color = 'green';
                                    document.getElementById('message').innerHTML = 'MATCHING PASSWORD';
                                    } else {
                                    document.getElementById('message').style.color = 'red';
                                    document.getElementById('message').innerHTML = 'NOT MATCHING PASSWORD';
                                    }
                                    }


                                    var slideIndex = 0;
                                    showSlides();

                                    function showSlides() {
                                    var i;
                                    var slides = document.getElementsByClassName("mySlides");
                                    var dots = document.getElementsByClassName("dot");
                                    for (i = 0; i < slides.length; i++) {
                                    slides[i].style.display = "none";
                                    }
                                    slideIndex++;
                                    if (slideIndex > slides.length) {slideIndex = 1}
                                    for (i = 0; i < dots.length; i++) {
                                    dots[i].className = dots[i].className.replace(" active", "");
                                    }
                                    slides[slideIndex-1].style.display = "block";
                                    dots[slideIndex-1].className += " active";
                                    setTimeout(showSlides, 2000); // Change image every 2 seconds
                                    }

                                    var myInput = document.getElementById("password");
                                    var letter = document.getElementById("letter");
                                    var capital = document.getElementById("capital");
                                    var number = document.getElementById("number");
                                    var length = document.getElementById("length");

                                    // When the user clicks on the password field, show the message box
                                    myInput.onfocus = function() {
                                    document.getElementById("message").style.display = "block";
                                    }

                                    // When the user clicks outside of the password field, hide the message box
                                    myInput.onblur = function() {
                                    document.getElementById("message").style.display = "none";
                                    }

                                    // When the user starts to type something inside the password field
                                    myInput.onkeyup = function() {
                                    // Validate lowercase letters
                                    var lowerCaseLetters = /[a-z]/g;
                                    if(myInput.value.match(lowerCaseLetters)) {
                                    letter.classList.remove("invalid");
                                    letter.classList.add("valid");
                                    } else {
                                    letter.classList.remove("valid");
                                    letter.classList.add("invalid");
                                    }

                                    // Validate capital letters
                                    var upperCaseLetters = /[A-Z]/g;
                                    if(myInput.value.match(upperCaseLetters)) {
                                    capital.classList.remove("invalid");
                                    capital.classList.add("valid");
                                    } else {
                                    capital.classList.remove("valid");
                                    capital.classList.add("invalid");
                                    }

                                    // Validate numbers
                                    var numbers = /[0-9]/g;
                                    if(myInput.value.match(numbers)) {
                                    number.classList.remove("invalid");
                                    number.classList.add("valid");
                                    } else {
                                    number.classList.remove("valid");
                                    number.classList.add("invalid");
                                    }

                                    // Validate length
                                    if(myInput.value.length >= 8) {
                                    length.classList.remove("invalid");
                                    length.classList.add("valid");
                                    } else {
                                    length.classList.remove("valid");
                                    length.classList.add("invalid");
                                    }
                                    }



                                    </script>


                                    <?php
                                    if(isset($_POST['submit']))
                                    {
                                    $rollno = $_GET['id'];
                                    $name=$_POST['Name'];
                                    $category=$_POST['Category'];
                                    $email=$_POST['EmailId'];
                                    $mobno=$_POST['MobNo'];
                                    $pswd=md5($_POST['Password']);
                                    

                                    $sql1="update LMS.user set Name='$name', Category='$category', EmailId='$email', MobNo='$mobno', Password='$pswd' where RollNo='$rollno'";



                                    if($conn->query($sql1) === TRUE){
                                    echo "<script type='text/javascript'>alert('Success')</script>";
                                    header( "Refresh:0.01; url=index.php", true, 303);
                                    }
                                    else
                                    {//echo $conn->error;
                                    echo "<script type='text/javascript'>alert('Error')</script>";
                                    }
                                    }
                                    ?>

                                    </body>

                                    </html>
